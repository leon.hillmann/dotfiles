vim.o.termguicolors = true
vim.cmd([[
set number relativenumber
set clipboard+=unnamedplus
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set cursorline
set colorcolumn=80
set tw=80
set noerrorbells
set smartindent
set incsearch
set nowrap
]])

-- require("p1zz4k4tz3/plugins")
-- require("p1zz4k4tz3/colors")
-- require("p1zzakatz3/mappings")
-- require("p1zzakatz3/telescope")
require'p1zz4k4tz3'
