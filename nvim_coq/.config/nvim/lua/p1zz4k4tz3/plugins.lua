return require('packer').startup(function()
  use 'wbthomason/packer.nvim'

  use 'neovim/nvim-lspconfig'

  use 'KurtPreston/vimcolors'
  use 'folke/tokyonight.nvim'

  use {'ms-jpq/coq_nvim', branch='coq'}
  use {'ms-jpq/coq.artifacts', branch='artifacts'}
  use {'ms-jpq/coq.thirdparty', branch = '3p'}
  
  use {'nvim-treesitter/nvim-treesitter', run=':TSUpdate'}

  -- Copilot <3
  use 'github/copilot.vim'

  use 'nvim-lua/popup.nvim'
  use 'nvim-lua/plenary.nvim'
  use 'nvim-telescope/telescope.nvim'
  use 'nvim-telescope/telescope-fzy-native.nvim'

  use 'jpalardy/vim-slime'
  vim.g.slime_target = "neovim"

  use 'tpope/vim-fugitive'

  -- -- use {'psf/black', branch='stable'}
  use 'averms/black-nvim'

  use 'junegunn/vim-easy-align'

  use 'Raimondi/delimitMate'

  -- Markdown preview
  use {
    'iamcco/markdown-preview.nvim', 
    run = function() vim.fn['mkdp#util#install']() end,
    ft = {'markdown'}
  }
  -- Seamless vim, tmux and i3 navigation
  use 'christoomey/vim-tmux-navigator'

  -- Airline (line that shows mode, filetype, git statues etc.)
  -- use 'itchyny/lightline.vim'
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }

  use 'mbbill/undotree'

  use 'jremmen/vim-ripgrep'

  -- use 'ludovicchabant/vim-gutentags'
  -- use 'rhysd/vim-clang-format'

  -- Editor config
  use 'gpanders/editorconfig.nvim'
  

  use 'vim-scripts/pylint.vim'

  use 'neomake/neomake'

  -- jdtls
  -- use 'mfussenegger/nvim-jdtls'

  -- Auto formatter
  use 'Chiel92/vim-autoformat'


  use 'ThePrimeagen/harpoon'
  use 'ThePrimeagen/refactoring.nvim'

  -- Git worktrees
  use 'ThePrimeagen/git-worktree.nvim'

  -- use 'JuliaEditorSupport/julia-vim'
  
  -- Vim Wiki
  use 'vimwiki/vimwiki'

  -- Cheat sheet!!!
  use 'dbeniamine/cheat.sh-vim'

  -- Surround
  use {
    "ur4ltz/surround.nvim",
    config = function()
      require"surround".setup {mappings_style = "sandwich"}
    end
  }
end)
