require("coq_3p") {
    { src = "nvimlua", short_name = "nLUA" },
    { src = "bc", short_name = "MATH", precision = 6 },
    { src = "copilot", short_name = "COP", accept_key = "<c-f>"}
}

vim.g.coq_settings = {
    auto_start = true,
    clients = { tabnine = {enabled = false}},
    keymap = { jump_to_mark = '<c-tab>'}
}
