
vim.g.mapleader = " "

-- Use ä and ö to jump paragraphs as sentences.
vim.api.nvim_set_keymap( "", "ö", "<S-{>", { silent = true })
vim.api.nvim_set_keymap( "", "ä", "<S-}>", { silent = true })
vim.api.nvim_set_keymap( "", "Ö", "<S-(>", { silent = true })
vim.api.nvim_set_keymap( "", "Ä", "<S-)>", { silent = true })

vim.api.nvim_set_keymap( "i", "<M-o>", "<Plug>(copilot-previous)", {})
vim.api.nvim_set_keymap( "i", "<M-p>", "<Plug>(copilot-next)", {})

-- Use ü instead of ^
vim.api.nvim_set_keymap( "n", "ü", "^", { silent = true })
vim.api.nvim_set_keymap( "n", "<leader>ü", "<C-^>", { silent = true })

-- Terminal
vim.api.nvim_set_keymap( "t", "<Esc>", "<C-\\><C-n>", { noremap = true })

-- no highlight
vim.api.nvim_set_keymap( "", "<leader>sd", "<cmd>:noh<CR>", { silent = true })

-- move text in visual mode
vim.cmd([[
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
]])

-- Fugitive
vim.api.nvim_set_keymap( "n", "<leader>gs", "<cmd>:G<CR>", { noremap = true } )
vim.api.nvim_set_keymap( "n", "<leader>gl", "<cmd>:diffget //2<CR>", { noremap = true } )
vim.api.nvim_set_keymap( "n", "<leader>gr", "<cmd>:diffget //3<CR>", { noremap = true } )

-- Git worktree
vim.api.nvim_set_keymap( "n", "<leader>gb", "<cmd>:lua require('telescope').extensions.git_worktree.git_worktrees()<CR>", { noremap = true } )
vim.api.nvim_set_keymap( "n", "<leader>gB", "<cmd>:lua require('telescope').extensions.git_worktree.create_git_worktree()<CR>", { noremap = true } )

-- Vimwiki
vim.cmd([[
autocmd Filetype vimwiki nnoremap <leader>note i#Explanation<CR><CR><CR><CR>#Documentation<CR><CR><CR><CR>#Code<CR><CR>```python<CR><CR>```<esc>ggji
autocmd Filetype vimwiki nnoremap <leader>date <cmd>r!date<CR>I## <esc>kddo
]])

vim.api.nvim_set_keymap( 
    "n",
    "<leader>w<leader>w",
    "<cmd>VimwikiIndex 1<CR><cmd>VimwikiMakeDiaryNote<CR>",
    { noremap=true }
)

-- tabs
vim.api.nvim_set_keymap( "n",
    "<leader>t",
    ":tabn<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>T",
    ":tabp<CR>",
    { noremap=true }
)

-- spell check
vim.api.nvim_set_keymap( "n",
    "gs",
    "]s",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "gS",
    "[s",
    { noremap=true }
)

-- harpoon
vim.api.nvim_set_keymap( "n",
    "<leader>m",
    ":lua require(\"harpoon.ui\").toggle_quick_menu()<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-m>",
    ":lua require(\"harpoon.mark\").add_file()<CR>",
    { noremap=true }
)

vim.api.nvim_set_keymap( "n",
    "<M-1>",
    ":lua require(\"harpoon.ui\").nav_file(1)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-2>",
    ":lua require(\"harpoon.ui\").nav_file(2)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-3>",
    ":lua require(\"harpoon.ui\").nav_file(3)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-4>",
    ":lua require(\"harpoon.ui\").nav_file(4)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-5>",
    ":lua require(\"harpoon.ui\").nav_file(5)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-6>",
    ":lua require(\"harpoon.ui\").nav_file(6)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-7>",
    ":lua require(\"harpoon.ui\").nav_file(7)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-8>",
    ":lua require(\"harpoon.ui\").nav_file(8)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-9>",
    ":lua require(\"harpoon.ui\").nav_file(9)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<M-0>",
    ":lua require(\"harpoon.ui\").nav_file(10)<CR>",
    { noremap=true }
)

vim.api.nvim_set_keymap( "n",
    "<leader>1",
    ":lua require(\"harpoon.term\").gotoTerminal(1)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>2",
    ":lua require(\"harpoon.term\").gotoTerminal(2)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>3",
    ":lua require(\"harpoon.term\").gotoTerminal(3)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>4",
    ":lua require(\"harpoon.term\").gotoTerminal(4)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>5",
    ":lua require(\"harpoon.term\").gotoTerminal(5)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>6",
    ":lua require(\"harpoon.term\").gotoTerminal(6)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>7",
    ":lua require(\"harpoon.term\").gotoTerminal(7)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>8",
    ":lua require(\"harpoon.term\").gotoTerminal(8)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>9",
    ":lua require(\"harpoon.term\").gotoTerminal(9)<CR>",
    { noremap=true }
)
vim.api.nvim_set_keymap( "n",
    "<leader>0",
    ":lua require(\"harpoon.term\").gotoTerminal(10)<CR>",
    { noremap=true }
)

