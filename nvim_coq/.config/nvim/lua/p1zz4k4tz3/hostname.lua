-- Provides a function to retrieve the machine's hostname

local _M = {}

function _M.getHostname()
  local f = io.popen("hostname")
  local hostname = f:read("*a")
  f:close()
  hostname =string.gsub(hostname, "\n$", "")
  return hostname
end

return _M
