vim.api.nvim_set_keymap( "n", "<leader>pp", "<cmd>Telescope find_files<CR>", {})
vim.api.nvim_set_keymap( "n", "<leader>fg", "<cmd>Telescope live_grep<CR>", {})
vim.api.nvim_set_keymap( "n", "<leader>fb", "<cmd>Telescope buffers<CR>", {})
vim.api.nvim_set_keymap( "n", "<leader>fh", "<cmd>Telescope help_tags<CR>", {})

local actions = require('telescope.actions')
require('telescope').setup {
    defaults = {
        file_sorter = require('telescope.sorters').get_fzy_sorter,
        prompt_prefix = ' >',
        color_devicons = true,

        file_previewer   = require('telescope.previewers').vim_buffer_cat.new,
        grep_previewer   = require('telescope.previewers').vim_buffer_vimgrep.new,
        qflist_previewer = require('telescope.previewers').vim_buffer_qflist.new,

        mappings = {
            i = {
                ["<C-q>"] = actions.send_to_qflist,
            },
        }
    },
    extensions = {
        fzy_native = {
            override_generic_sorter = false,
            override_file_sorter = true,
        }
    }
}

require('telescope').load_extension('fzy_native')

return M
