vim.cmd([[
autocmd BufWritePre *.py silent! execute ':call BlackSync()'
autocmd BufWritePre *.rs silent! execute ':Autoformat'
autocmd BufWritePre *.cc silent! execute ':Autoformat'
autocmd BufWritePre *.cu silent! execute ':Autoformat'
autocmd BufWritePre *.h silent! execute ':Autoformat'
]])
