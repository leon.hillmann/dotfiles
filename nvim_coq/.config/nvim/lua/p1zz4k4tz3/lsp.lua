
local opts = { noremap=true, silent=true }
vim.api.nvim_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
vim.api.nvim_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)

local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  -- vim.api.nvim_buf_set_keymap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<M-TAB>', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
  
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>dq', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>dn', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>dp', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
end

local pid = vim.fn.getpid()
local lsp = require "lspconfig"
local coq = require "coq"

local omnisharp_bin = "/home/leon/.local/share/nvim/lsp_servers/omnisharp/omnisharp/run"

-- lsp.pylsp.setup(coq.lsp_ensure_capabilities{ 
--     on_attach = on_attach,
--     settings = {
--         pylsp = {
--             plugins = {
--                 jedi_completion = {
--                     cache_for = {"matplotlib", "numpy"}
--                 }
--             }
--         }
--     }
-- })
lsp.pyright.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

lsp.hls.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

lsp.cmake.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

lsp.texlab.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

lsp.rust_analyzer.setup(coq.lsp_ensure_capabilities{
    on_attach = on_attach,
    settings = {
        ["rust-analyzer"] =  {
            checkOnSave = {
                command = "clippy"
            },
        }
    }
})

lsp.elmls.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

-- lsp.clangd.setup{
--     on_attach = on_attach,
--     filetypes={ "c", "cpp", "objc", "objcpp", "cuda"},
-- }
lsp.clangd.setup(coq.lsp_ensure_capabilities{ 
    on_attach = on_attach,
    filetypes={ "c", "cpp", "objc", "objcpp", "cuda"},
})

-- lsp.omnisharp.setup { 
--     on_attach = on_attach,
--     cmd = { omnisharp_bin, "--languageserver" , "--hostPID", tostring(pid) };
-- }
lsp.omnisharp.setup(coq.lsp_ensure_capabilities{ 
    on_attach = on_attach,
    cmd = { omnisharp_bin, "--languageserver" , "--hostPID", tostring(pid) };
})

lsp.jdtls.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

lsp.gopls.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

local function create_capabilities()
  local capabilities = vim.lsp.protocol.make_client_capabilities()
  capabilities.textDocument.completion.completionItem.snippetSupport = true
  capabilities.textDocument.completion.completionItem.preselectSupport = true
  capabilities.textDocument.completion.completionItem.tagSupport = { valueSet = { 1 } }
  capabilities.textDocument.completion.completionItem.deprecatedSupport = true
  capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
  capabilities.textDocument.completion.completionItem.labelDetailsSupport = true
  capabilities.textDocument.completion.completionItem.commitCharactersSupport = true
  capabilities.textDocument.completion.completionItem.resolveSupport = {
    properties = { "documentation", "detail", "additionalTextEdits" },
  }
  capabilities.textDocument.completion.completionItem.documentationFormat = { "markdown" }
  capabilities.textDocument.codeAction = {
    dynamicRegistration = true,
    codeActionLiteralSupport = {
      codeActionKind = {
        valueSet = (function()
          local res = vim.tbl_values(vim.lsp.protocol.CodeActionKind)
          table.sort(res)
          return res
        end)(),
      },
    },
  }
  return capabilities
end

lsp.julials.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach, capabilities = create_capabilities() })

lsp.html.setup(coq.lsp_ensure_capabilities{ on_attach = on_attach })

vim.lsp.set_log_level("debug")
