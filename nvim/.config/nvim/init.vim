
" Set line numbers to hybrid
set number relativenumber
set nu rnu

" use system clipboard
set clipboard+=unnamedplus

" Syntax highlighting in json files
autocmd FileType json syntax match Comment +\/\/.\+$+

" Change tabstop to be 4 spaces
" show existing tab with 4 spaces width
set tabstop=4 softtabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" Highight current line
set cursorline

set colorcolumn=80
setl tw=80
set noerrorbells
set smartindent
set incsearch

set nowrap

let g:doom_one_terminal_colors = v:true

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

	" coc (conquer of completion)
	" Plug 'neoclide/coc.nvim', {'branch': 'release'}
	" Extended coc config
	" source $HOME/.config/nvim/config/coc.vim
    
    " Neovim lsp
    Plug 'neovim/nvim-lspconfig'
    Plug 'mfussenegger/nvim-jdtls'
    Plug 'williamboman/nvim-lsp-installer'

    " auto completion
    Plug 'nvim-lua/completion-nvim'

    " Tree sitter
    Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate'}

    Plug 'kien/ctrlp.vim'
    let g:ctrlp_show_hidden=1
    " Julia editor support
    " Plug 'JuliaEditorSupport/julia-vim'
    
    " Git Plugin
    Plug 'tpope/vim-fugitive'

    " Line number switching (absolute insertion, hybrid else)
    ""Plug 'jeffkreeftmeijer/vim-numbertoggle'
    
    " Color scheme
    Plug 'KurtPreston/vimcolors'
    Plug 'arcticicestudio/nord-vim'
    Plug 'drewtempelmeyer/palenight.vim'
    Plug 'romgrk/doom-one.vim'
    " Plug 'kyazdani42/nvim-palenight.lua'

    " Snippets
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'

    " Vim slime (send code to tmux pane)
    Plug 'jpalardy/vim-slime'
    " let g:slime_target = "tmux"
    let g:slime_target = "neovim"
    
    " Code formatting for python
    Plug 'psf/black', { 'branch': 'stable' }
    " source $HOME/.config/nvim/config/black.vim
    
    " Incsearch (better searching)
    ""Plug 'haya14busa/incsearch.vim'
    ""source $HOME/.config/nvim/config/incsearch.vim

    Plug 'junegunn/vim-easy-align'


    Plug 'Raimondi/delimitMate'
    " Markdown preview
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

    " Seamless vim, tmux and i3 navigation
    Plug 'christoomey/vim-tmux-navigator'

    " Airline (line that shows mode, filetype, git statues etc.)
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

    Plug 'mbbill/undotree'

    Plug 'jremmen/vim-ripgrep'

    Plug 'ludovicchabant/vim-gutentags'
    ""Plug 'rhysd/vim-clang-format'
    

    Plug 'neomake/neomake'
    
    " Auto formatter
    Plug 'Chiel92/vim-autoformat'


    Plug 'vim-scripts/pylint.vim'

    Plug 'neomake/neomake'

    " jdtls
    Plug 'mfussenegger/nvim-jdtls'

    " Auto formatter
    Plug 'Chiel92/vim-autoformat'

    Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }


    " Telescope fuzzy finder
    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'nvim-telescope/telescope-fzy-native.nvim'


    Plug 'ThePrimeagen/harpoon'

    " Git worktrees
    Plug 'ThePrimeagen/git-worktree.nvim'

    " Plug 'hoob3rt/lualine.nvim'


    Plug 'JuliaEditorSupport/julia-vim'
    
    " Vim Wiki
    Plug 'vimwiki/vimwiki'

    " Cheat sheet!!!
    Plug 'dbeniamine/cheat.sh-vim'

    " Rusty object notation syntax plugin
    Plug 'ron-rs/ron.vim'

" Initialize plugin system
call plug#end()
"
" disable julia plugin native tab completion for latex letters
" let g:latex_to_unicode_tab = 0

" Ignore the following file types:
set wildignore+=*.out,*.o,*.png

" Custom keyboard bindings and configuration
source $HOME/.config/nvim/config/keybindings.vim
source $HOME/.config/nvim/config/black.vim
"" source $HOME/.config/nvim/config/clang-format.vim
source /home/leon/.config/nvim/config/format.vim
source /home/leon/.config/nvim/config/gutentags.vim
" source $HOME/.config/nvim/config/ctrl-p.vim
source /home/leon/.config/nvim/config/telescope.vim

" Saved macros:
" source $HOME/.config/nvim/config/macros.vim

" set colorscheme
set termguicolors
" set bg=light
" colorscheme gruvbox
" colorscheme nord

set background=dark
colorscheme palenight
let g:airline_theme = "palenight"
" let g:palenight_terminal_italics=1
colorscheme doom-one

set completeopt=menuone,noinsert,noselect
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']
let g:completion_enable_snippet = 'UltiSnips'

" VimWiki config
" let g:vimwiki_folding='expr'

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

lua require'p1zz4k4tz3'

" LSP config
lua << EOF 
require'lspconfig'.pyright.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.clangd.setup{ on_attach=require'completion'.on_attach, filetypes = { "c", "cpp", "objc", "objcpp", "cuda"}}
require'lspconfig'.elmls.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.cmake.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.rust_analyzer.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.texlab.setup{ on_attach=require'completion'.on_attach }
-- vim.lsp.set_log_level("debug")
EOF 

" Tree sitter config
"
" Indentation
lua <<EOF
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    custom_captures = {
      -- Highlight the @foo.bar capture group with the "Identifier" highlight group.
      ["foo.bar"] = "Identifier",
    },
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
EOF

set completeopt=menuone,noinsert,noselect
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']
let g:UltiSnipsSnippetDirectories=["/home/leon/.config/nvim/UltiSnips", "/home/leon/.vim/plugged/vim-snippets/UltiSnips/"]
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:completion_enable_snippet = 'UltiSnips'

let g:JAR="/usr/share/java/jdtls/plugins/org.eclipse.equinox.launcher_1.6.0.v20200915-1508.jar"
let g:JDTLS_CONFIG="/usr/share/java/jdtls/config_linux"
let g:WORKSPACE="/home/leon/workspace"

" Don't auto close MarkdownPreview
let g:mkdp_auto_close = 0

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" LSP config
lua << EOF 
local pid = vim.fn.getpid()

local omnisharp_bin = "/home/leon/.local/share/nvim/lsp_servers/omnisharp/omnisharp/run"

require'lspconfig'.pyright.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.hls.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.clangd.setup{
    on_attach=require'completion'.on_attach, filetypes={ "c", "cpp", "objc", "objcpp", "cuda"}
}
require'lspconfig'.texlab.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.elmls.setup{ on_attach=require'completion'.on_attach }
require'lspconfig'.omnisharp.setup { 
    cmd = { omnisharp_bin, "--languageserver" , "--hostPID", tostring(pid) };
    on_attach=require'completion'.on_attach
}
EOF 

" Window size workaround
autocmd VimEnter * :silent exec "!kill -s SIGWINCH $PPID"

" jdtls
if has('nvim-0.5')
  augroup lsp
    au!
    au FileType java lua require('jdtls').start_or_attach({cmd = {'start_jdtls.sh'}, on_attach=require'completion'.on_attach})
  augroup end
endif

" Window size workaround
autocmd VimEnter * :silent exec "!kill -s SIGWINCH $PPID"

" folding
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set nofoldenable
" set foldnestmax=2
"
let g:vimwiki_list = [{
  \ 'path': '$HOME/vimwiki',
  \ 'template_path': '$HOME/vimwiki/templates',
  \ 'template_default': 'default',
  \ 'template_ext': '.html'}]

let g:neomake_python_enabled_makers = ['pylint']
" let g:neomake_c_enabled_makers = ['clang-tidy']


let g:vimwiki_list = [
\ {'path': '$HOME/vimwiki',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'}, 
\ {'path': '$HOME/vimwiki/master',
\ 'path_html': '$HOME/vimwiki_html/master',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/MD-Simulation',
\ 'path_html': '$HOME/vimwiki_html/master/MD-Simulation',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/HOOMD',
\ 'path_html': '$HOME/vimwiki_html/master/HOOMD',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/HOOMD3',
\ 'path_html': '$HOME/vimwiki_html/master/HOOMD3',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/cmake',
\ 'path_html': '$HOME/vimwiki_html/master/cmake',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/fresnel',
\ 'path_html': '$HOME/vimwiki_html/master/fresnel',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/scmp',
\ 'path_html': '$HOME/vimwiki_html/master/scmp',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/literature',
\ 'path_html': '$HOME/vimwiki_html/master/literature',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'},
\ {'path': '$HOME/vimwiki/master/hpc',
\ 'path_html': '$HOME/vimwiki_html/master/hpc',
\ 'template_path': '$HOME/vimwiki/templates',
\ 'template_default': 'default',
\ 'template_ext': '.html'}]


" autocmd BufWritePost *.py silent! execute ':Neomake'
" autocmd BufEnter *.py silent! execute ':Neomake'

autocmd Filetype elm setlocal tabstop=2
autocmd Filetype elm setlocal shiftwidth=2

autocmd Filetype vimwiki setlocal spell
