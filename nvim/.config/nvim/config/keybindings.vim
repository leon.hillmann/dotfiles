" set the leader to space
let mapleader=" "


" Coc Bindings
" Remap keys for gotos
" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <C-]>

nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
" nnoremap <silent> gi    <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
nnoremap <silent> gD    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <M-TAB> <cmd>lua vim.lsp.buf.code_action()<CR>

nnoremap <leader>dq <cmd>lua vim.diagnostic.setloclist()<CR>
nnoremap <leader>dn <cmd>lua vim.diagnostic.goto_next()<CR>
nnoremap <leader>dp <cmd>lua vim.diagnostic.goto_prev()<CR>

autocmd Filetype vimwiki nnoremap <leader>note i=Explanation=<CR><CR><CR><CR>=Documentation=<CR><CR><CR><CR>=Code=<CR><CR>{{{python<CR><CR>}}}<esc>ggji
autocmd Filetype vimwiki nnoremap <leader>date <cmd>r!date<CR>I==<esc>A==<esc>o

" Reference keybindings in vimwiki
autocmd Filetype vimwiki nnoremap <leader>rjones Go[]: https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=jones+soft+condensed+matter&btnG=<esc>0a

" Let there be only one global diary instead of one for each subwiki
" (might want to change this in the future...)
nnoremap <leader>w<leader>w <cmd>VimwikiIndex 1<CR><cmd>VimwikiMakeDiaryNote<CR>

command! Diary VimwikiDiaryIndex
augroup vimwikigroup
    autocmd!
    " automatically update links on read diary
    autocmd BufRead,BufNewFile diary.wiki VimwikiDiaryGenerateLinks
augroup end

" nmap <silent> gr <Plug>(coc-references)

" Let F2 be rename
" nmap <F2> <Plug>(coc-rename)
nnoremap <silent> <F2> <cmd>lua vim.lsp.buf.rename()<CR>
" 
" let g:coc_snippet_next = '<tab>'
" let g:coc_snippet_prev = '<S-tab>'


" Reset current selection from search buffer
map <leader>sd :noh<CR>

" Use leader-P instead of Ctrl-P
let g:ctrlp_map = ''
nnoremap <leader>pp :CtrlP<CR>

" Map <F5> to run files in a terminal (dependent on mode)
" autocmd FileType python map <F5> <Esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>
" autocmd FileType python map <F5> <Esc>:w<CR>:CocCommand python.execInTerminal<CR>
" autocmd FileType julia  map <F5> <Esc>:w<CR>:exec '!julia' shellescape(@%, 1)<CR>
" autocmd FileType tex    map <F1> <Esc>:w<CR>:CocCommand latex.Build<CR>

" autocmd BufWritePost *.tex silent! execute ':CocCommand latex.Build'

" nnoremap <M-tab> <Esc>:CocAction<CR>

" Map ga as prefix for EasyAlign
" xmap <leader>ga <Plug>(EasyAlign)
" nmap <leader>ga <Plug>(EasyAlign)

" Tmux and i3 navigation
" let g:tmux_navigator_no_mappings = 1

" nnoremap <silent> <C-h> :TmuxNavigateLeft<cr>
" nnoremap <silent> <C-j> :TmuxNavigateDown<cr>
" nnoremap <silent> <C-k> :TmuxNavigateUp<cr>
" nnoremap <silent> <C-l> :TmuxNavigateRight<cr>
" nnoremap <silent> <C-\> :TmuxNavigatePrevious<cr>

" Use ö and ü to move paragraphs
noremap ö <C-{>
noremap ä <C-}>
noremap Ö (
noremap Ä )


" Use ü instead of ^
noremap ü ^
nnoremap <leader>ü <C-^>

" Go to file
nnoremap <leader>gf :vertical wincmd f<CR>

nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30 <CR>
nnoremap <leader>+ :vertical resize +5 <CR>
nnoremap <leader>- :vertical resize -5 <CR>
nnoremap <leader><leader> :exe "vertical resize " . (&columns * 1/2)<CR>
nnoremap <leader>{ :exe "vertical resize " . (&columns * 1/3)<CR>
nnoremap <leader>} :exe "vertical resize " . (&columns * 2/3)<CR>
" nnoremap <leader>u :UndotreeToggle<CR>
" nnoremap <leader>go :CocList diagnostics<CR>
nnoremap <leader>gs :G<CR>
" Git merge, get left or get right
nnoremap <leader>gl :diffget //2<CR>
nnoremap <leader>gr :diffget //3<CR>
nnoremap <leader>psw :Rg <C-R>=expand("<cword>")<CR><CR>
nnoremap <leader>ps :Rg<Space>
nnoremap <silent> <leader>gb :lua require('telescope').extensions.git_worktree.git_worktrees()<CR>
nnoremap <silent> <leader>gB :lua require('telescope').extensions.git_worktree.create_git_worktree()<CR>
" nnoremap <leader>prw :CocSearch <C-R>=expand("<cword>")<CR><CR>

" Remap Ctrl-p to Ctrl-i to create a working jump list again
nnoremap <C-p> <C-i>

nnoremap <leader>t :tabn<CR>
nnoremap <leader>T :tabp<CR>
nnoremap <leader>bn :bn<CR>
nnoremap <leader>bp :bp<CR>
nnoremap <leader>bd :bd<CR>

nnoremap <leader>l :lne<CR>
nnoremap <leader>L :lp<CR>
nnoremap <C-n> :cne<CR>
nnoremap <C-b> :cp<CR>

tnoremap <Esc> <C-\><C-n>

" Harpoon configuration
nnoremap <leader>m :lua require("harpoon.ui").toggle_quick_menu()<CR>
nnoremap <M-m> :lua require("harpoon.mark").add_file()<CR>
nnoremap <M-1> :lua require("harpoon.ui").nav_file(1)<CR>
nnoremap <M-2> :lua require("harpoon.ui").nav_file(2)<CR>
nnoremap <M-3> :lua require("harpoon.ui").nav_file(3)<CR>
nnoremap <M-4> :lua require("harpoon.ui").nav_file(4)<CR>
nnoremap <M-5> :lua require("harpoon.ui").nav_file(5)<CR>
nnoremap <M-6> :lua require("harpoon.ui").nav_file(6)<CR>
nnoremap <M-7> :lua require("harpoon.ui").nav_file(7)<CR>
nnoremap <M-8> :lua require("harpoon.ui").nav_file(8)<CR>
nnoremap <M-9> :lua require("harpoon.ui").nav_file(9)<CR>
nnoremap <leader>1 :lua require("harpoon.term").gotoTerminal(1)<CR>
nnoremap <leader>2 :lua require("harpoon.term").gotoTerminal(2)<CR>
nnoremap <leader>3 :lua require("harpoon.term").gotoTerminal(3)<CR>
nnoremap <leader>4 :lua require("harpoon.term").gotoTerminal(4)<CR>
nnoremap <leader>5 :lua require("harpoon.term").gotoTerminal(5)<CR>
nnoremap <leader>6 :lua require("harpoon.term").gotoTerminal(6)<CR>
nnoremap <leader>7 :lua require("harpoon.term").gotoTerminal(7)<CR>
nnoremap <leader>8 :lua require("harpoon.term").gotoTerminal(8)<CR>
nnoremap <leader>9 :lua require("harpoon.term").gotoTerminal(9)<CR>
