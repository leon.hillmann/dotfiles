lua require("p1zz4k4tz3")

lua require('telescope').load_extension("git_worktree")

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>pp <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
