" Configuration for the black python formatting tool

let g:black_linelength=79
set noshowmode

autocmd BufWritePost *.py silent! execute ':Black'
