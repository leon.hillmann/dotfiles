autocmd BufWritePost *.cc silent! execute ':Autoformat'
autocmd BufWritePost *.h  silent! execute ':Autoformat'
" autocmd BufWritePost *.py  silent! execute ':Autoformat'

autocmd BufWritePost *.json  silent! execute ':Autoformat'
autocmd BufWritePost *.rs silent! execute ':Autoformat'
" autocmd BufWritePost *.rs silent! :execute '!rustfmt %' | e % | w


let g:black_linelength=79
set noshowmode

autocmd BufWritePost *.py silent! execute ':Black'
