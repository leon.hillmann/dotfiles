--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--

import Data.List
import Data.Maybe (fromJust)
import Network.HostName

import XMonad
import System.Exit

import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops -- To use rofi as a window switcher
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.DynamicProperty

import XMonad.Layout.Spacing

import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad

import XMonad.Actions.CopyWindow -- Add DWM like tagging
import XMonad.Actions.CycleWS
import XMonad.Actions.RotSlaves
import XMonad.Actions.Search as S
import XMonad.Actions.SwapPromote
import XMonad.Actions.NoBorders
import XMonad.Actions.SpawnOn

import qualified XMonad.Prompt   as P
import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import Data.Monoid
import XMonad.Prompt
import XMonad.Layout.Renamed
import XMonad.Layout.NoBorders
import XMonad.Layout.WindowNavigation
import XMonad.Layout.Tabbed
import XMonad.Layout.SubLayouts
import XMonad.Layout.Simplest
import XMonad.Layout.Magnifier
import XMonad.Layout.LimitWindows
import XMonad.Layout.ResizableTile
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Grid
import XMonad.Layout.CenteredMaster
import XMonad.Actions.DwmPromote
import Data.Conduit.Process (createProcess)

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "brave"

myFont :: String
myFont = "xft:SauceCodePro Nerd Font Mono:regular:size=9:antialias=true:hinting=true"


-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["dev","web","doc","vid","sys","6","mus","chat","mail", "scratch"]
myWorkspaceIndices = M.fromList $ zip myWorkspaces [1..] -- (,) == \x y -> (x,y)

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Custom function definitions
------------------------------------------------------------------------

(~?) :: (Eq a, Functor m) => m [a] -> [a] -> m Bool
q ~? x = fmap (x `isInfixOf`) q


------------------------------------------------------------------------
-- XPROMPT SETTINGS
------------------------------------------------------------------------
myXPConfig :: XPConfig
myXPConfig = def
      { font                = "xft:Mononoki Nerd Font:size=12"
      , bgColor             = "#292d3e"
      , fgColor             = "#d0d0d0"
      , bgHLight            = "#c792ea"
      , fgHLight            = "#000000"
      , borderColor         = "#535974"
      , promptBorderWidth   = 0
      , position            = Top
--    , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
--    , autoComplete        = Just 100000  -- set Just 100000 for .1 sec
      , autoComplete        = Nothing      -- Do not auto-select the last remaining entry
      , showCompletionOnTab = False
      , searchPredicate     = isPrefixOf
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to Just 5 for 5 rows
      }



--Make setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw True (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }



------------------------------------------------------------------------
-- Layouts:

tall        = renamed [Replace "tall"]
              $ mySpacing 4
              $ Tall 1 (3/100) (1/2)

grid        = renamed [Replace "grid"]
              $ mySpacing 4
              $ GridRatio (16/10)

myMagnify     = renamed [Replace "magnify"]
              $ mySpacing 4
              $ magnifier
              $ ResizableTall 1 (3/100) (1/2) []


magnifyGrid = renamed [Replace "grid magnify"]
              $ mySpacing 4
              $ magnifier
              $ GridRatio (16/10)


monocle     = renamed [Replace "monocle"]
              $ mySpacing' 4
              $ centerMaster Grid

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = tall ||| myMagnify ||| grid ||| monocle ||| magnifyGrid ||| Mirror tall ||| Mirror myMagnify ||| Full

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore 
    , resource  =? "matplotlib"     --> doFloat
    , className =? "wired"          --> doIgnore
    , className =? "xtrkcad"          --> doIgnore
    , title =? "pixels"     --> doFloat
    , title =? "SFML window"     --> doFloat
    , className =? " "     --> doFloat
    , className =? "TelegramDesktop" --> doShift(myWorkspaces !! 7)
    , className =? "okular" --> doShift(myWorkspaces !! 2)
    , className =? "Thunderbird" --> doShift(myWorkspaces !! 8)
    , className =? "Brave-browser" --> doShift "web"
    , className =? "firefox" --> doShift "web"
    , resource =? "mpsyt" --> doShift(myWorkspaces !! 6)
    ] <+> namedScratchpadManageHook myScratchPads

myDynamicManageHook = composeAll
    [ title ~? "WhatsApp"     --> doShift "chat"
    , title ~? "Spotify"      --> doShift "mus"
    ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
-- myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
------------------------------------------------------------------------

-- Make workspaces clickable
-- clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
--     where i = fromJust $ M.lookup ws myWorkspaceIndices

myLogHookAx370 xmproc1 xmproc2 = dynamicLogWithPP . filterOutWsPP ["scratch"] $ xmobarPP
                    { ppOutput = \x -> hPutStrLn xmproc1 x                          -- xmobar on monitor 1
                                    >> hPutStrLn xmproc2 x
                    , ppCurrent = xmobarColor "#c792ea" "" . wrap "[ " " ]"         -- Current workspace
                    , ppVisible = xmobarColor "#c792ea" "" -- . clickable              -- Visible but not current workspace
                    , ppHidden = xmobarColor "#b2e68c" ""  -- . clickable -- Hidden workspaces
                    , ppHiddenNoWindows = xmobarColor "#82AAFF" ""  -- . clickable     -- Hidden workspaces (no windows)
                    , ppTitle = xmobarColor "#b3afc2" "" . shorten 60               -- Title of active window
                    , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                    -- Separator character
                    , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"            -- Urgent workspace
                    , ppOrder  = \(ws:l:t:ex) -> [ws, l]++ex++[t]                    -- order of things in xmobar
              }

myLogHook xmproc1 = dynamicLogWithPP . filterOutWsPP ["scratch"] $ xmobarPP
                    { ppOutput = hPutStrLn xmproc1                          -- xmobar on monitor 1
                    , ppCurrent = xmobarColor "#c792ea" "" . wrap "[ " " ]"         -- Current workspace
                    , ppVisible = xmobarColor "#c792ea" "" -- . clickable              -- Visible but not current workspace
                    , ppHidden = xmobarColor "#b2e68c" ""  -- . clickable -- Hidden workspaces
                    , ppHiddenNoWindows = xmobarColor "#82AAFF" ""  -- . clickable     -- Hidden workspaces (no windows)
                    , ppTitle = xmobarColor "#b3afc2" "" . shorten 60               -- Title of active window
                    , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                    -- Separator character
                    , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"            -- Urgent workspace
                    , ppOrder  = \(ws:l:t:ex) -> [ws, l]++ex++[t]                    -- order of things in xmobar
              }
------------------------------------------------------------------------
-- Search configuration
------------------------------------------------------------------------
searchList :: [(String, S.SearchEngine)]
searchList = [ ("g", intelligent S.google)
             , ("s", S.scholar)
             , ("y", S.youtube)
             , ("i", S.images)
             , ("w", S.wikipedia)
             , ("z", S.searchEngine "amazon.de" "https://www.amazon.de/s?k=")
             , ("m", S.maps)
             , ("v", S.searchEngine "Cambridge dictionary" "https://dictionary.cambridge.org/dictionary/english/")
             , ("d", S.searchEngine "duden.de" "https://www.duden.de/suchen/dudenonline/")
             ]


------------------------------------------------------------------------
-- Scratch pads
------------------------------------------------------------------------

myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "units" spawnUnits findUnits manageTerm
                , NS "mixer" spawnMixer findMixer manageMixer
                , NS "htop" spawnHtop findHtop manageTerm
                , NS "wiki" spawnWiki findWiki manageTerm
                , NS "diary" spawnDiary findDiary manageTerm
                , NS "python" spawnPython findPython manageTerm
                , NS "helvum" spawnHelvum findHelvum manageTerm
                , NS "git-check" spawnGitCheck findGitCheck manageTerm
                ]
    where
    spawnTerm  = myTerminal ++ " --class scratchpad"
    findTerm   = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
                 where
                 h = 0.9
                 w = 0.9
                 t = 1/2 - h / 2
                 l = 1/2 - w / 2
    spawnUnits = myTerminal ++ " --class unitsPad -e units"
    findUnits  = resource =? "unitsPad"
    spawnMixer = myTerminal ++ " --class audioMixer -e pulsemixer"
    findMixer  = resource =? "audioMixer"
    manageMixer = customFloating $ W.RationalRect l t w h
                 where
                 h = 1/2
                 w = 1/2
                 t = 1/2 - h / 2
                 l = 1/2 - w / 2
    spawnHtop  = myTerminal ++ " --class htopPad -e htop"
    findHtop   = resource =? "htopPad"
    spawnWiki  = myTerminal ++ " --class wikiPad -e nvim $HOME/obsidian/Notes/index.md"
    findWiki   = resource =? "wikiPad"
    spawnDiary  = myTerminal ++ " --class diaryPad -e nvim $HOME/obsidian/Notes/index.md +VimwikiMakeDiaryNote"
    findDiary   = resource =? "diaryPad"
    spawnPython = myTerminal ++ " --class pythonPad -e tmux new -s pyscratch bpython"
    findPython  = resource =? "pythonPad"
    spawnHelvum = "helvum"
    findHelvum = resource =? "helvum"
    spawnGitCheck = myTerminal ++ " --class gitCheckPad"
    findGitCheck = resource =? "gitCheckPad"

        
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
    spawnOnce "stalonetray"
    spawnOnce "lxsession &"
    spawnOnce "picom"
    spawnOnce "nitrogen --restore &"
    spawnOnce "$HOME/bin/remap"
    spawnOnce "nm-applet &"
    spawnOnce "seafile-applet &"
    spawnOnce "seadrive-gui &"
    spawnOnce "owncloud &"
    spawnOnce "keepassxc &"
    spawnOnce "xfce4-clipman &"
    spawnOnce "wired &"
    spawn "$HOME/bin/git_pull_hook.sh"


mySuspend :: IO [Char]
mySuspend = do
    hostname <- getHostName
    case hostname of
        "leon-laptop" -> return "/home/leon/bin/blurlock && systemctl suspend-then-hibernate"
        _ -> return "/home/leon/bin/blurlock && systemctl suspend"

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys :: [(String, X ())]
myKeys = 
    -- KB Group Xmonad
    [ ("M-S-q", io exitSuccess)
    , ("M-q", spawn "killall xmobar; xmonad --recompile; xmonad --restart")
    -- KB Group Launch Programs   
    , ("M-<Return>", spawn (myTerminal ++ " -e $HOME/bin/start_tmux"))
    , ("M-<F1>", spawn myBrowser)
    , ("M-<F3>", spawn "pcmanfm")
    , ("M-m", spawn (myTerminal ++ " --class mpsyt -e mpsyt"))

    -- KB Group windows
    , ("M1-<F4>", kill1)
    , ("M-S-<Return>", whenX (swapHybrid True) dwmpromote)
    , ("M-C-j", rotAllUp)
    , ("M-C-k", rotAllDown)

    , ("M1-<Space>", spawn "rofi -combi-modi window,drun,ssh -show combi")
    , ("C-M1-<Space>", spawn "fd -t f -t d --exclude SeaDrive/ --exclude .seadrive/ --exclude .seafile/ --exclude venv/ --exclude rpi_backup/ --no-ignore-vcs | rofi -dmenu -matching normal -i -p 'find:' | xargs -r -0 xdg-open")

    -- KB Group Workspaces
    , ("M-.", nextScreen)
    , ("M-,", prevScreen)
    , ("M-S-<Space>", sendMessage ToggleStruts)
    , ("M-b", withFocused toggleBorder)

    -- KB Group Session
    , ("M-w w", spawnWork)

    -- KB Group git
    , ("M-g f", spawn "$HOME/bin/git_pull_hook.sh")
    , ("M-g p", spawn "$HOME/bin/git_hook.sh")

    -- KB Group system commands
    , ("M-0 l", spawn "/home/leon/bin/blurlock")
    , ("M-0 s", liftIO mySuspend >>= spawn)
    , ("M-0 S-s", spawn "$HOME/bin/git_hook.sh && systemctl poweroff")
    , ("M-0 r", spawn "systemctl reboot")
    , ("M-0 e", io exitSuccess)

    -- KB Group scratchPads
    , ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
    , ("M-p u", namedScratchpadAction myScratchPads "units")
    , ("M-p h", namedScratchpadAction myScratchPads "htop")
    , ("M-p m", namedScratchpadAction myScratchPads "mixer")
    , ("M-p w", namedScratchpadAction myScratchPads "wiki")
    , ("M-p d", namedScratchpadAction myScratchPads "diary")
    , ("M-p p", namedScratchpadAction myScratchPads "python")
    , ("M-p b", namedScratchpadAction myScratchPads "helvum")

    -- KB Group multimedia
    , ("<XF86MonBrightnessUp>", spawn "xbacklight -inc 10; notify-send 'brightness up'")
    , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 10; notify-send 'brightness down'")
    , ("<XF86AudioLowerVolume>", spawn "pulsemixer --change-volume -2")
    , ("<XF86AudioRaiseVolume>", spawn "pulsemixer --change-volume +2")
    , ("<XF86AudioNext>", spawn "echo playlist-next | socat - ~/.mpv_socket/socket")
    , ("<XF86AudioPrev>", spawn "echo playlist-prev | socat - ~/.mpv_socket/socket")
    , ("<XF86AudioPlay>", spawn "echo cycle pause | socat - ~/.mpv_socket/socket")
    , ("<XF86AudioStop>", spawn "echo quit | socat - ~/.mpv_socket/socket")
    
    -- KB Group prompts
    , ("M-d", spawn "dmenu_run -i -p \"Run: \"")

    , ("M-+", windows $ W.greedyView "1")
    , ("M-[", windows $ W.greedyView "2")
    , ("M-{", windows $ W.greedyView "3")
    , ("M-(", windows $ W.greedyView "4")
    , ("M-&", windows $ W.greedyView "5")
    , ("M-=", windows $ W.greedyView "6")
    , ("M-)", windows $ W.greedyView "7")
    , ("M-}", windows $ W.greedyView "8")
    , ("M-]", windows $ W.greedyView "9")

    , ("M-S-+", windows $ W.shift "1")
    , ("M-S-[", windows $ W.shift "2")
    , ("M-S-{", windows $ W.shift "3")
    , ("M-S-(", windows $ W.shift "4")
    , ("M-S-&", windows $ W.shift "5")
    , ("M-S-=", windows $ W.shift "6")
    , ("M-S-)", windows $ W.shift "7")
    , ("M-S-}", windows $ W.shift "8")
    , ("M-S-]", windows $ W.shift "9")
    -- KB Group copy windows
    ] 
    ++ [("M-C-S-" ++ show i, windows $ copy (show i)) | i <- [1..9]]
    ++ [("M-s " ++ k, S.promptSearchBrowser myXPConfig myBrowser f) | (k, f) <- searchList]
    ++ [("M-S-s " ++ k, S.selectSearchBrowser myBrowser f) | (k, f) <- searchList]
    

------------------------------------------------------------------------
-- Workspace spawners

spawnWork :: X()
spawnWork = do
    runProcessWithInput "killall" ["brave"] ""
    runProcessWithInput "killall" ["alacritty"] ""
    runProcessWithInput "killall" ["telegram-desktop"] ""
    runProcessWithInput "killall" ["thunderbird"] ""
    spawnOn "mus" "brave --new-window https://open.spotify.com"
    spawnOn "chat" "brave --new-window https://web.whatsapp.com"
    spawnOn "chat" "telegram-desktop"
    spawnOn "mail" "thunderbird"
    spawnOn "web" "brave --new-window"
    spawnOn "dev" (myTerminal ++ " -e $HOME/bin/start_tmux")


------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do 
    hostname <- getHostName
    case hostname of
        "leon-ax370" -> do
            xmproc1 <- spawnPipe "xmobar -x 0 /home/leon/.config/xmobar/xmobarrc0"
            xmproc2 <- spawnPipe "xmobar -x 1 /home/leon/.config/xmobar/xmobarrc1"
            xmonad $ ewmh $ docks (defaults_ax370 xmproc1 xmproc2)
        _ -> do
        xmproc1 <- spawnPipe "xmobar -x 0 /home/leon/.config/xmobar/xmobarrc_laptop"
        xmonad $ ewmh $ docks (defaults xmproc1)
    

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults_ax370 xmproc1 xmproc2 = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
      -- keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = avoidStruts myLayout,
        manageHook         = myManageHook <+> manageSpawn,
        handleEventHook    = dynamicPropertyChange "WM_NAME" myDynamicManageHook,
        logHook            = myLogHookAx370 xmproc1 xmproc2 >> masterHistoryHook,
        startupHook        = myStartupHook
    } `additionalKeysP` myKeys

defaults xmprocs = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
      -- keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = avoidStruts myLayout,
        manageHook         = myManageHook <+> manageSpawn,
        handleEventHook    = dynamicPropertyChange "WM_NAME" myDynamicManageHook,
        logHook            = myLogHook xmprocs >> masterHistoryHook,
        startupHook        = myStartupHook
    } `additionalKeysP` myKeys


-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
