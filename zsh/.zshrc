# Use powerline
USE_POWERLINE="false"
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt SHARE_HISTORY


autoload -Uz compinit
compinit
zstyle ':completion::complete:*' gain-privileges 1
zstyle ':completion:*' menu select
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

export PATH=$HOME/.cargo/bin/:$HOME/bin:$HOME/.local/bin:$HOME/.emacs.d/bin:$HOME/.ghcup/bin:$PATH

# set nvim as default visual editor
export VISUAL=/usr/bin/nvim
# export VISUAL=/usr/bin/neovide

# Nvim java LSP variables
# export JAR=/usr/share/java/jdtls/plugins/org.eclipse.equinox.launcher_1.6.0.v20200915-1508.jar
# export GRADLE_HOME=$HOME/gradle
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk/
export _JAVA_AWT_WM_NONREPARENTING=1
# export JDTLS_CONFIG=/usr/share/java/jdtls/config_linux
# export WORKSPACE=$HOME/workspace
export NXJ_HOME=/opt/lejos-nxj
export LEJOS_NXT_JAVA_HOME=/usr/lib/jvm/java-7-openjdk


# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias sl="sl -e"
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles/.git --work-tree=$HOME/.dotfiles"
alias wiki="/usr/bin/git --git-dir=$HOME/vimwiki/.git --work-tree=$HOME/vimwiki"
# alias ls="ls -lh --color"

# Rust replacement programs:
alias ls="exa -l --group-directories-first"
alias cat=bat
alias less=bat


alias em="emacsclient -c"
alias wakeax="wakeonlan E0:D5:5E:83:18:63"
alias wakespit="wakeonlan -i 134.102.132.140 -p 9 E0:D5:5E:83:18:63"
# alias matlab="matlab -softwareopengl"
alias gn="systemctl suspend-then-hibernate"
alias e="$VISUAL"
alias :e="$VISUAL"
alias edit=e
alias f=fuck
alias op='devour xdg-open'
alias open='devour xdg-open'
alias fe='nvim $(fzf)'
alias fo='devour xdg-open "$(fzf)"'
alias ipy="python -c 'import IPython; IPython.terminal.ipapp.launch_new_instance()'"

# Taskwarrior aliases
alias in="task add +in"
tickle () {
    deadline=$1
    shift
    in +tickle wait:$deadline $@
}
alias tick=tickle

webpage_title (){
    curl -qLs "$*" | sed -n '/<head>/,/<\/head>/p' | grep '<title>' | sed "s/<[^<>]*>//g"
}

read_and_review (){
    link="$1"
    title=$(webpage_title $link)
    echo $title
    descr="\"Read and review: $title\""
    id=$(task add +next +rnr "$descr" | sed -n 's/Created task \(.*\)./\1/p')
    task "$id" annotate "$link"
}

alias rnr=read_and_review

alias plan="task context plan"


rga-fzf() {
	RG_PREFIX="rga --files-with-matches"
	local file
	file="$(
		FZF_DEFAULT_COMMAND="$RG_PREFIX '$1'" \
			fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
				--phony -q "$1" \
				--bind "change:reload:$RG_PREFIX {q}" \
				--preview-window="70%:wrap"
	)" &&
	echo "opening $file" &&
	xdg-open "$file"
}
export FZF_ALT_C_COMMAND="fd -t d -E opt -E SeaDrive . $HOME"

# if command -v pyenv 1>/dev/null 2>&1; then
#   eval "$(pyenv init -)"
# fi
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

eval $(thefuck --alias)

export PYENV_VIRTUALENV_DISABLE_PROMPT=1
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

eval "$(jump shell)"
eval "$(starship init zsh)"
