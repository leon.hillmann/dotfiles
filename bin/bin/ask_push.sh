#!/bin/bash
GIT_DIR=$1
WORK_DIR=$2

export VISUAL=nvim

function my_git() {
    git --git-dir=$GIT_DIR --work-tree=$WORK_DIR $@
}

read -p "Do you want to push to $1? (y/n)" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    my_git push
fi
