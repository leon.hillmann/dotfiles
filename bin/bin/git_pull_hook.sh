#!/bin/bash

task sync

# check for config & diary changes
GIT_DIR=$HOME/vimwiki/.git
WORK_DIR=$HOME/vimwiki
$HOME/bin/git_fetch_check $GIT_DIR $WORK_DIR

GIT_DIR=$HOME/obsidian/Notes/.git
WORK_DIR=$HOME/obsidian/Notes
$HOME/bin/git_fetch_check $GIT_DIR $WORK_DIR

GIT_DIR=$HOME/.dotfiles/.git
WORK_DIR=$HOME/.dotfiles
$HOME/bin/git_fetch_check $GIT_DIR $WORK_DIR

