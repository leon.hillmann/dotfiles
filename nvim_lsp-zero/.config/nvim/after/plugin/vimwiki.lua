vim.g.vimwiki_list = {
    {
        path = "$HOME/obsidian/Notes",
        syntax = "markdown",
        ext = ".md",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/MD-Simulation",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/MD-Simulation",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/HOOMD",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/HOOMD",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/HOOMD3",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/HOOMD3",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/cmake",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/cmake",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/fresnel",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/fresnel",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/scmp",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/scmp",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
    {
        path = "$HOME/obsidian/Notes/master/literature",
        syntax = "markdown",
        ext = ".md",
        path_html = "$HOME/vimwiki_html/master/literature",
        template_path = "$HOME/obsidian/Notes/templates",
        template_default = "default",
        template_ext = ".html"
    },
}

