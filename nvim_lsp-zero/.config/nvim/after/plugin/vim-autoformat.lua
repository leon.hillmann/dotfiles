vim.cmd([[
autocmd BufWritePre *.rs silent! execute ':Autoformat'
autocmd BufWritePre *.cc silent! execute ':Autoformat'
autocmd BufWritePre *.cu silent! execute ':Autoformat'
autocmd BufWritePre *.h silent! execute ':Autoformat'
]])

