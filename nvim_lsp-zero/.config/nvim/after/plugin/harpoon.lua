local mark = require("harpoon.mark")
local ui = require("harpoon.ui")
local term = require("harpoon.term")

vim.keymap.set("n", "<leader>a", mark.add_file)
vim.keymap.set("n", "<C-e>", ui.toggle_quick_menu)

vim.keymap.set("n", "<M-1>", function() ui.nav_file(1) end)
vim.keymap.set("n", "<M-2>", function() ui.nav_file(2) end)
vim.keymap.set("n", "<M-3>", function() ui.nav_file(3) end)
vim.keymap.set("n", "<M-4>", function() ui.nav_file(4) end)
vim.keymap.set("n", "<M-5>", function() ui.nav_file(5) end)
vim.keymap.set("n", "<M-6>", function() ui.nav_file(6) end)
vim.keymap.set("n", "<M-7>", function() ui.nav_file(7) end)
vim.keymap.set("n", "<M-8>", function() ui.nav_file(8) end)
vim.keymap.set("n", "<M-9>", function() ui.nav_file(9) end)

vim.keymap.set("n", "<leader>1", function() term.gotoTerminal(1) end)
vim.keymap.set("n", "<leader>2", function() term.gotoTerminal(2) end)
vim.keymap.set("n", "<leader>3", function() term.gotoTerminal(3) end)
vim.keymap.set("n", "<leader>4", function() term.gotoTerminal(4) end)
vim.keymap.set("n", "<leader>5", function() term.gotoTerminal(5) end)
vim.keymap.set("n", "<leader>6", function() term.gotoTerminal(6) end)
vim.keymap.set("n", "<leader>7", function() term.gotoTerminal(7) end)
vim.keymap.set("n", "<leader>8", function() term.gotoTerminal(8) end)
vim.keymap.set("n", "<leader>9", function() term.gotoTerminal(9) end)
