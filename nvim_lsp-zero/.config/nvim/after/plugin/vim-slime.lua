vim.g.slime_target = "neovim"

-- Nushell fix
--
-- Override the SlimeSend function according to 
-- https://github.com/jpalardy/vim-slime#how-to-override-send-to-target
-- such that the `cat > ` part of the command is converted to 
-- `save --raw --force` as specified in 
-- https://nushell.sh/book/coming_from_bash.html

vim.cmd([[
function! OverrideWritePasteFile(text)
  let paste_dir = fnamemodify(g:slime_paste_file, ":p:h")
  if !isdirectory(paste_dir)
    call mkdir(paste_dir, "p")
  endif
  let output = system("cat | save --raw --force " . shellescape(g:slime_paste_file), a:text)
  if v:shell_error
    echoerr output
  endif
endfunction


function SlimeOverrideSend(config, text)
  " Neovim jobsend is fully asynchronous, it causes some problems with
  " iPython %cpaste (input buffering: not all lines sent over)
  " So this s:WritePasteFile can help as a small lock & delay
  call OverrideWritePasteFile(a:text)
  call chansend(str2nr(a:config["jobid"]), split(a:text, "\n", 1))
  " if b:slime_config is {"jobid": ""} and not configured
  " then unset it for automatic configuration next time
  if b:slime_config["jobid"]  == ""
      unlet b:slime_config
  endif
endfunction
]])
