-- Reserve space for diagnostic icons
vim.opt.signcolumn = 'yes'

local lsp = require("lsp-zero")

lsp.preset("recommended")
lsp.setup()

lsp.ensure_installed({
	'rust_analyzer',
	'clangd',
	'pyright',
})

lsp.configure('tsserver', {
  settings = {
	  ["rust-analyzer"] = {
		checkOnSave = {
			command = "clippy"
		},
	  },
  },
})

lsp.configure('clangd', {
	filetypes={ "c", "cpp", "objc", "objcpp", "cuda"},
})

lsp.nvim_workspace()

lsp.on_attach(function(client, bufnr)
	local opts = {buffer = bufnr, remap = false}

	vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)

  	-- Mappings.
  	vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end, opts)
  	vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
  	vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
  	vim.keymap.set("n", "gi", function() vim.lsp.buf.implementation() end, opts)
  	vim.keymap.set("n", "<leader>wa", function() vim.lsp.buf.add_workspace_folder() end, opts)
  	vim.keymap.set("n", "<leader>wr", function() vim.lsp.buf.remove_workspace_folder() end, opts)
  	vim.keymap.set("n", "<leader>wl", function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts)
  	vim.keymap.set("n", "<leader>D", function() vim.lsp.buf.type_definition() end, opts)
  	vim.keymap.set("n", "<leader>rn", function() vim.lsp.buf.rename() end, opts)
  	vim.keymap.set("n", "<F2>", function() vim.lsp.buf.rename() end, opts)
  	vim.keymap.set("n", "<M-TAB>", function() vim.lsp.buf.code_action() end, opts)
  	vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end, opts)
  	vim.keymap.set("n", "<leader>f", function() vim.lsp.buf.formatting() end, opts)
  	vim.keymap.set("n", "<leader>dq", function() vim.diagnostic.setloclist() end, opts)
  	vim.keymap.set("n", "<leader>dn", function() vim.diagnostic.goto_next() end, opts)
  	vim.keymap.set("n", "<leader>dp", function() vim.diagnostic.goto_prev() end, opts)
	vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
end)

lsp.setup_nvim_cmp({
  sources = {
    {name = 'path'},
    {name = 'nvim_lsp', keyword_length = 3},
    {name = 'buffer', keyword_length = 3},
    {name = 'luasnip', keyword_length = 2},
    {name = 'copilot' }
  }
})

lsp.setup()
