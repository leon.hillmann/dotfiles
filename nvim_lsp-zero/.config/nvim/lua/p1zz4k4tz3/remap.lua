vim.api.nvim_set_keymap( "", "ö", "<S-{>zz", { silent = true })
vim.api.nvim_set_keymap( "", "ä", "<S-}>zz", { silent = true })
vim.api.nvim_set_keymap( "", "Ö", "<S-(>zz", { silent = true })
vim.api.nvim_set_keymap( "", "Ä", "<S-)>zz", { silent = true })

vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Use ü instead of ^
vim.api.nvim_set_keymap( "n", "ü", "^", { silent = true })
vim.api.nvim_set_keymap( "n", "<leader>ü", "<C-^>", { silent = true })

-- Terminal
vim.api.nvim_set_keymap( "t", "<Esc>", "<C-\\><C-n>", { noremap = true })


-- Move visually selected lines up or down with J and K
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("x", "<leader>p", "\"_dP")

-- use leader as a prefix for yank to yank into system clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- delete to void register when prefixing d with leader
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

vim.keymap.set("n", "Q", "<nop>")

-- quickfix list and loclist navigation
vim.keymap.set("n", "<C-n>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-p>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<C-M-n>", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<C-M-p>", "<cmd>lprev<CR>zz")

vim.keymap.set("n", "<leader>s", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- vimwiki bindings
-- Vimwiki
vim.cmd([[
autocmd Filetype vimwiki nnoremap <leader>note i#Explanation<CR><CR><CR><CR>#Documentation<CR><CR><CR><CR>#Code<CR><CR>```python<CR><CR>```<esc>ggji
autocmd Filetype vimwiki nnoremap <leader>date <cmd>r!date<CR>I## <esc>kddo
]])

